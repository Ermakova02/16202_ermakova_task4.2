package ru.nsu.ermakova.stackcalculator;

public class CommandDefine extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        if (args.size() != 2) throw new ArgumentsNumberException("Количество аргументов не равно двум");
        Double num = Double.valueOf(args.get(1));
        cntx.list.put(args.get(0), num);
    }
}
