package ru.nsu.ermakova.stackcalculator;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class StackCalculator {
    public static final String EXIT_LINE = "EXIT";
    public static final char COMMENT_SIGN = '#';
    public static final char SPACE_SIGN = ' ';
    private boolean consoleInput;
    private String inputFileName;
    public StackCalculator() {
        consoleInput = true;
        inputFileName = "";
    }
    public void setConsoleInput()
    {
        consoleInput = true;
        inputFileName = "";
    }
    public void setFileInput(String name) {
        consoleInput = false;
        inputFileName = name;
    }
    public void process(){
        Scanner in = null;
        if (consoleInput) {
            in = new Scanner(System.in);
        }
        else {
            try {
                in = new Scanner(new FileReader(inputFileName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        String line = "";
        String substr = "";
        String cmdName = "";
        Command cmd = null;
        ArrayList<String> args = new ArrayList<>();
        CalculatorContext cntx = new CalculatorContext();
        CommandFactory factory = new CommandFactory();
        while (in.hasNext()) {
            line = in.nextLine();
            if (line.equals(EXIT_LINE)) break;
            if (line.length() == 0) continue;
            if (line.charAt(0) == COMMENT_SIGN) continue;
            int beginIndex = line.indexOf(SPACE_SIGN);
            int endIndex = 0;
            args.clear();
            if (beginIndex == -1) {
                cmdName = line;
            }
            else {
                cmdName = line.substring(0, beginIndex);
                do {
                    beginIndex++;
                    endIndex = line.indexOf(SPACE_SIGN, beginIndex);
                    if (endIndex == -1) substr = line.substring(beginIndex);
                    else substr = line.substring(beginIndex, endIndex);
                    if (substr.length() > 0) args.add(substr);
                    beginIndex = endIndex;
                } while (beginIndex != -1);
            }
            try {
                cmd = factory.getCommand(cmdName);
                cmd.setArguments(args);
                cmd.setContext(cntx);
                cmd.execute();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (CommandNotFoundException e) {
                System.out.println("Ошибка: " + e.getMessage());
            } catch (ArgumentsNumberException e) {
                System.out.println("Ошибка: " + e.getMessage());
            } catch (EmptyStackException e) {
                System.out.println("Ошибка: " + e.getMessage());
            } catch (MathException e) {
                System.out.println("Ошибка: " + e.getMessage());
            } catch (DefineException e) {
                System.out.println("Ошибка: " + e.getMessage());
            } catch (ArithmeticException e) {
                System.out.println("Ошибка: " + e.getMessage());
            } catch (NumberFormatException e) {
                System.out.println("Ошибка: " + e.getMessage());
            }
        }
    }
}
