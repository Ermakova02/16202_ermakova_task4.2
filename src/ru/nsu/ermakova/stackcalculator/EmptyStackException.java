package ru.nsu.ermakova.stackcalculator;

public class EmptyStackException extends Exception {
    public EmptyStackException(String message) {
        super(message);
    }
}
