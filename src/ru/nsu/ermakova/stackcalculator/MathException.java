package ru.nsu.ermakova.stackcalculator;

public class MathException extends Exception {
    public MathException(String message) {
        super(message);
    }
}
