package ru.nsu.ermakova.stackcalculator;

public class CommandSubtraction extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException{
        if (args.size() != 0) throw new ArgumentsNumberException("Количество аргументов не равно нулю");
        if (cntx.stack.isEmpty()) throw new EmptyStackException("Стек пустой");
        Double num1 = cntx.stack.pop();
        if (cntx.stack.isEmpty()) throw new EmptyStackException("Стек пустой");
        Double num2 = cntx.stack.pop();
        num1 -= num2;
        cntx.stack.push(num1);
    }
}
