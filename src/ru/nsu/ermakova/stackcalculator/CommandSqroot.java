package ru.nsu.ermakova.stackcalculator;

public class CommandSqroot extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        if (args.size() != 0) throw new ArgumentsNumberException("Количество аргументов не равно нулю");
        if (cntx.stack.isEmpty()) throw new EmptyStackException("Стек пустой");
        Double num = cntx.stack.pop();
        if (num < 0.0) throw new MathException("Извлечение корня из отрицательного числа");
        num = Math.sqrt(num);
        cntx.stack.push(num);
    }
}
