package ru.nsu.ermakova.stackcalculator;

public class CommandPop extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        if (args.size() != 0) throw new ArgumentsNumberException("Количество аргументов не равно нулю");
        if (cntx.stack.isEmpty()) throw new EmptyStackException("Стек пустой");
        cntx.stack.pop();
    }
}
