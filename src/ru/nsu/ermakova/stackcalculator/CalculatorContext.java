package ru.nsu.ermakova.stackcalculator;

import java.util.HashMap;
import java.util.Map;

public class CalculatorContext {
    public Map<String, Double> list;
    public NumericStack stack;
    CalculatorContext() {
        list = new HashMap();
        stack = new NumericStack();
    }
}
