package ru.nsu.ermakova.stackcalculator;

import java.util.ArrayList;

public abstract class Command {
    protected ArrayList<String> args;
    protected CalculatorContext cntx;

    public abstract void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException;

    public void setArguments(ArrayList<String> args) {
        this.args = args;
    }
    public void setContext(CalculatorContext cntx) {
        this.cntx = cntx;
    }
}
