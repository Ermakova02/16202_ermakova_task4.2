package ru.nsu.ermakova.stackcalculator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CommandFactory {
    public Command getCommand(String cmdName) throws ClassNotFoundException, CommandNotFoundException {
        Properties prop = new Properties();
        String className = "";
        Command cmd = null;
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("ru/nsu/ermakova/stackcalculator/config.properties");
            prop.load(inputStream);
            className = prop.getProperty(cmdName);
            if (className == null)
                throw new CommandNotFoundException("Не найдена команда " + cmdName);
            cmd = (Command) Class.forName(className).newInstance();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
/*        Command cmd = null;
        switch(cmdName) {
            case "POP": cmd = new CommandPop(); break;
            case "PUSH": cmd = new CommandPush(); break;
            case "+": cmd = new CommandAddition(); break;
            case "-": cmd = new CommandSubtraction(); break;
            case "*": cmd = new CommandMultiplication(); break;
            case "/": cmd = new CommandDivision(); break;
            case "SQRT": cmd = new CommandSqroot(); break;
            case "PRINT": cmd = new CommandPrint(); break;
            case "DEFINE": cmd = new CommandDefine(); break;
        }*/
        return cmd;
    }
}
