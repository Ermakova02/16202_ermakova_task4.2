package ru.nsu.ermakova.stackcalculator;

public class NumericStack {
    private Node top;
    NumericStack() {
        top = null;
    }
    public void push(Double n) {
        Node newNode = new Node();
        newNode.num = n;
        newNode.next = top;
        top = newNode;
    }
    public Double pop() {
        if (top == null) return 0.0;
        Double num = top.num;
        top = top.next;
        return num;
    }
    public Double getTopValue() {
        if (top == null) return 0.0;
        return top.num;
    }
    public boolean isEmpty() {
        if (top == null) return true;
        return false;
    }
    public class Node {
        public Double num;
        public Node next;
        Node() {
            num = 0.0;
            next = null;
        }
    }
}
