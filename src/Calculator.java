
import ru.nsu.ermakova.stackcalculator.StackCalculator;

public class Calculator {
    public static void main(String[] args) {
        StackCalculator calc = new StackCalculator();
        if (args.length > 0) calc.setFileInput(args[0]);
        else calc.setConsoleInput();
        calc.process();
    }
}
